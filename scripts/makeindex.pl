#!/usr/local/bin/perl
#
# Rebuilds the GDBM index
#

# WebTodo, a cgi-based todolist system
# Copyright (C) 1999-2001 British Broadcasting Corporation
# by Ciaran Anscomb <ciarana@rd.bbc.co.uk>,
#    Chris Turner <chris-tl@gathering.org.uk>
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# See the COPYING file for details.

use lib "/usr/local/todo/lib";
use Todo::Config;
use Todo::Task;
use Todo::List;
use Fcntl;
use GDBM_File;

$subdir="";
if ($ARGV[0] eq "-old") {
	$subdir="/old";
	print "For OLD index: ";
}

print "Rebuilding index\n";
unlink "$Todo::Config::tasks_dir$subdir/$Todo::Config::dbmfile";
if (Todo::List::open_todolist("$Todo::Config::tasks_dir$subdir", $Todo::Config::dbmfile, 1)) {
	die "tie: $!";
}

if(opendir(DIR, "$Todo::Config::tasks_dir$subdir")) {
	foreach $file (readdir(DIR)) {
		if ( $file =~ /task.([-0-9]+)/) {
			$id=$1;
			#print "Adding $id\n" if ($debug);
			print STDERR "." if ($debug);
			$task = new Todo::Task();
			$task->read_task($Todo::Config::tasks_dir."$subdir/task.".$id);
			$owner=$task->list;
			$subject = Todo::General::escape($task->subject);
			$custemail = Todo::General::escape($task->custemail);
			$mtime=$task->mtime;
			$state=$task->state;
			$state = 1 if ($state == 0);
			$due=$task->bbe;
			$priority=$task->priority;
			$accesslist=Todo::General::escape($task->accesslist);
			$Todo::List::dbm{$id}="$owner:$subject:$custemail:$mtime:$state:$due:$priority:$accesslist";
			$counts{"$owner"}++;
		}
	}
} else {
	print "opendir: ",$Todo::Config::tasks_dir,"%subdir: $!\n";
}
Todo::List::close_todolist();
#untie %index;
chown((getpwnam($Todo::Config::httpd_user))[2], (getgrnam($Todo::Config::task_group))[2], "$Todo::Config::tasks_dir$subdir/$Todo::Config::dbmfile");
chmod 0660, "$Todo::Config::tasks_dir$subdir/$Todo::Config::dbmfile";
print STDERR "\n" if ($debug);
foreach $i (keys %counts) {
	print "$i has ".$counts{"$i"}." tasks.\n";
}
print "Done\n";
