# WebTodo, a cgi-based todolist system
# Copyright (C) 1999-2000 British Broadcasting Corporation
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# See the COPYING file for details.

# reads a template file and creates one or more subs called print_section,
# (s/section/whatever/) to be called by the main program.  nifty!

# the "gross hack" mentioned later is to strip out all content marked
# by @{ifnotguest},@{finotguest} so that guests can't even see the source
# to what they can't use.

use Todo::Config;

sub read_template {
	my ($esub,$addto);
	return 0 if ($_[0] =~ m#^/# || $_[0] =~ m#\./#);
	return 0 unless open(F, "$Todo::Config::tmpl_dir/$_[0]");
	$handle = $_[1];
	while (<F>) {
		if (/^\@section/) {
			chop;
			($_,$sect) = split;
			$esub = "sub print_$sect { print $handle '";
		} elsif (/^\@end/) {
			$esub .= "'; }";
			# gross hack alert!
			$esub =~ s/\${ifnotguest}.*\${finotguest}//sg if ($isguest);
			$esub =~ s/\${ifguest}.*\${figuest}//sg if (!$isguest);
			eval $esub if ($sect);
			$sect = "";
		} else {
			$addto = $_;
			$addto =~ s/\\/\\\\/g;
			$addto =~ s/'/\\'/g;
			$addto =~ s/\@\@/\@/g;
			$addto =~ s/\@{([^}]*)}/',\${$1},'/g;
			$esub .= $addto;
		}
	}
	close(F);
	return 1;
}

1;
