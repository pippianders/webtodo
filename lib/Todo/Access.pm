# this package is for things that control access to lists - permissions, etc
#
# currently only block/allow to certain lists is done

package Todo::Access;

use Todo::Config;

my %groupdata;

BEGIN
	{
	local *GROUP;
	
	if (open(GROUP, $Todo::Config::htgroup))
		{
		while(<GROUP>)
			{
			chop;
			($gname,$gmembers,$gaccess)=split(/:/);
			foreach $member (split(",",$gmembers))
				{
				$groupdata{$gname}->{"members"}->{$member}=1;
				}
			foreach $member (split(",",$gaccess))
				{
				$groupdata{$gname}->{"access"}->{$member}=1;
				}
			$groupdata{$gname}->{"members list"}=$gmembers;
			$groupdata{$gname}->{"access list"}=$gaccess;
			}
		close(GROUP);
		}
	}

sub check_accesslist
	{
	my $accessgroup=shift;
	my $user=shift;

	if($user eq $accessgroup)
		{
		return 1;
		}
	if(!defined($groupdata{$accessgroup}))
		{
		return 0;
		}
	if($groupdata{$accessgroup}->{"access list"} eq "")
		{
		return 0;
		}
	return check_list($groupdata{$accessgroup}->{"access list"},$user);
	}

sub check_membership
	{
	my $accessgroup=shift;
	my $user=shift;

	if($user eq $accessgroup)
		{
		return 1;
		}
	if(!defined($groupdata{$accessgroup}))
		{
		return 0;
		}
	if($groupdata{$accessgroup}->{"members list"} eq "")
		{
		return 0;
		}
	return check_list($groupdata{$accessgroup}->{"members list"},$user);
	}

sub check_list
	{
	my $access=shift;
	my $user=shift;
	my $match=-1;

	if($access eq "")
		{
		return 0;
		}
	foreach $amember (split(",",$access))
		{
		# Check they're _NOT_ in this group
		if( $amember =~ /^!(.*)/ )
			{
			my $temp;
			$amember=$1;
			$temp=1;
			if($amember eq $user)
				{
				$temp=0;
				}
			else
				{
				if(check_membership($amember,$user))
					{
					$temp=0;
					}
				}
			if($temp==0)
				{
				$match=0;
				}
			elsif($match==-1)
				{
				$match=1;
				}
			}
		else
			{
			if( $amember eq $user)
				{
				$match=1;
				}
			else
				{
				if(check_membership($amember,$user))
					{
					$match=1;
					}
				elsif($match==-1)
					{
					$match=0;
					}
				}
			}
		}
	if($match==-1)
		{
		$match=0;
		}
	return $match;
	}

sub list_groups
	{
	my $username=shift;
	my @groups;
	my $group;
	foreach $group (keys %groupdata)
		{
		if(check_membership($group,$username))
			{
			$groups[$#groups+1]=$group;
			}
		}
	return @groups;
	}

sub list_access
	{
	my $username=shift;
	my @groups;
	my $group;
	foreach $group (keys %groupdata)
		{
		if((check_accesslist($group,$username))||(check_membership($group,$username)))
			{
			$groups[$#groups+1]=$group;
			}
		}
	return @groups;
	}

sub _list_members
	{
	my $group=shift;
	my $username=shift;
	my @members;
	
	foreach $subgroup (keys %{$groupdata{$group}->{"members"}})
		{
		$members[$#members+1]=$subgroup;
		foreach (_list_members($subgroup,$username))
			{
			$members[$#members+1]=$_;
			}
		}
	return @members;
	}



sub list_members
	{
	my $group=shift;
	my $username=shift;
	my @members;
	
	if(check_membership($group,$username))
		{
		@members=_list_members($group,$username);
		}
	return @members;
	}

sub list_assign
	{
	my $username=shift;
	my $group;
	my @groups;
	my $subgroup;
	foreach $group (list_groups($username))
		{
		foreach $subgroup (keys %groupdata)
			{
			if((check_accesslist($group,$subgroup))||(check_membership($group,$subgroup)))
				{
				$groups[$#groups+1]=$subgroup;
				}
			}
		}
	return @groups;
	}
1;
