package Todo::List;

use GDBM_File;
use Todo::Task;
use Todo::General;

my $lastlist="self";

# open_todolist will tie to the gdbm file found in the specified dir
# it won't read it at all - use read_todolist

sub open_todolist {
	$dir = shift;
	my $fname = shift;
	my $towrite = shift;
	my $ret;
	my $index_lock = 30;

	if ($towrite) {   # then we have to lock it
		return 1 if ($dbmlocked);
		$ret = tie(%dbm, "GDBM_File", "$dir/$fname", &GDBM_WRCREAT, 0660);
		while(ref($ret) eq "") {
			if ($index_lock < 1) {
				print STDERR "Todo/List.pm: cannot lock index: $!\n";
				return 1;
			}
			$index_lock++;
			sleep(1);
			$ret = tie(%dbm, "GDBM_File", "$dir/$fname", &GDBM_WRCREAT, 0660);
		}
		$dbmlocked = 1;
	} else {
		$ret = tie(%dbm, "GDBM_File", "$dir/$fname", &GDBM_READER, 0660);
		if (ref($ret) eq "") {
			print STDERR "XXX: tieing $dir/$fname: $!\n";
			return 1;
		}
	}
	$dbmopen = 1;
	return 0;
}

# close_todolist basically just unties the dbm file

sub close_todolist {
	if ($dbmopen) {
		untie %dbm;
		$dbmopen = undef;
		$dbmlocked = undef;
	}
}

sub read_task {
	my ($id, $fullinfo,$user) = @_;
	my ($assigned,$subject,$email,$mtime,$state,$due,$priority,$accesslist);

	($assigned,$subject,$email,$mtime,$state,$due,$priority,$accesslist)=
		split(/:/,$dbm{$id});
	$subject = Todo::General::unescape($subject);
	$email   = Todo::General::unescape($email);
	$accesslist   = Todo::General::unescape($accesslist);
#	return if ($limit_assigned && ($assigned !~ /$limit_assigned/i));
#	return if (!$limit_subject_neg && $limit_subject && ($subject !~ /$limit_subject/i));
#	return if ($limit_subject_neg && $limit_subject && ($subject =~ /$limit_subject/i));
#	return if ($limit_email && ($email !~ /$limit_email/i));
#	return if ($limit_donetime && ($state==3) && ((time-$mtime) >= $limit_donetime));
	$task{$id} = new Todo::Task($id);
	$task{$id}->{'list'} = $assigned;
	$task{$id}->{'subject'} = $subject;
	$task{$id}->{'custemail'} = $email;
	$task{$id}->{'mtime'} = $mtime;
	$task{$id}->{'state'} = $state;
	$task{$id}->{'bbe'} = $due;
	$task{$id}->{'priority'} = $priority;
	$task{$id}->{'accesslist'} = $accesslist;
	push @useids, $id;
	return $id if (!$fullinfo);
	if ($task{$id}->read_task("$dir/task.$id",$user)) {
		$lastlist = $task{$id}->list;  # TODO: look into this
		return $id;
	} else {
		return 0;
	}
}

# read_todolist scans the dbm file, applying any set limits (with read_task).
# it will then close the dbm file and optionally read in the full information
# of any applicable tasks
#
# fullinfo = 1 to actually read task files
#
# TODO: include priority and due date in dbm so we have all useful sort info
#       currently should always call this with fullinfo=1
# TODO: add some permissions stuff to the limits

sub read_todolist {
	my $fullinfo = shift;
	my $user=shift;
	my ($assigned,$subject,$email,$mtime,$state,$due,$priority,$accesslist);
	my @useids, @tempids, $newtask;

	@tempids = keys %dbm;
	foreach $id (@tempids) {
#		read_task($id, 0);
	($assigned,$subject,$email,$mtime,$state,$due,$priority,$accesslist)=
		split(/:/,$dbm{$id});
	$subject = Todo::General::unescape($subject);
	$email   = Todo::General::unescape($email);
	$accesslist   = Todo::General::unescape($accesslist);
	next if ($limit_assigned && (Todo::Access::check_list($limit_assigned,$assigned)==0));
	next if (!$limit_subject_neg && $limit_subject && ($subject !~ /$limit_subject/i));
	next if ($limit_subject_neg && $limit_subject && ($subject =~ /$limit_subject/i));
	next if ($limit_email && ($email !~ /$limit_email/i));
	next if ($limit_donetime && ($state==3) && ((time-$mtime) >= $limit_donetime));
	$task{$id} = new Todo::Task($id);
	$task{$id}->{'list'} = $assigned;
	$task{$id}->{'subject'} = $subject;
	$task{$id}->{'custemail'} = $email;
	$task{$id}->{'mtime'} = $mtime;
	$task{$id}->{'state'} = $state;
	$task{$id}->{'bbe'} = $due;
	$task{$id}->{'priority'} = $priority;
	$task{$id}->{'accesslist'} = $accesslist;
	push @useids, $id;

	}
	close_todolist();  # release the dbm as early as possible
	return if (!$fullinfo);
	foreach $id (keys %task) {
		$task{$id}->read_task("$dir/task.$id",$user);
	}
}

# TODO: why is this needed?
sub lastlist { return $lastlist; }

# iterate_tasks begins iterating through all the tasks that were read in

sub iterate_tasks {
	$index = -1;
	$last = $#sortedids;
}

# next_task iterates to the next task and returns its data

sub next_task {
	return 0 if (++$index > $last);
	return $task{$sortedids[$index]};
}

sub limit_assigned {
	$limit_assigned = shift;
}

sub limit_subject {
	$limit_subject = shift;
	if ($limit_subject =~ /^!/) {
		$limit_subject =~ s/^!//;
		$limit_subject_neg = 1;
	} else {
		$limit_subject_neg = 0;
	}
}

sub limit_email {
	$limit_email = shift;
}

sub limit_donetime {
	$limit_donetime = shift;
}


# set_taskorder takes a sort format string and constructs a function (using
# eval) to do the comparisons
#
sub set_taskorder {
	my $sortsub = 'sub byformat { return ';
	while ($_[0] =~ /([umpbsta])/ig) {
		if ($1 eq 'm') {
			$sortsub .= '$task{$a}->{"mtime"} <=> $task{$b}->{"mtime"}';
		} elsif ($1 eq 'M') {
			$sortsub .= '$task{$b}->{"mtime"} <=> $task{$a}->{"mtime"}';
		} elsif ($1 eq 'a') {
			$sortsub .= 'lc($task{$a}->{"subject"}) cmp lc($task{$b}->{"subject"})';
		} elsif ($1 eq 'A') {
			$sortsub .= 'lc($task{$b}->{"subject"}) cmp lc($task{$a}->{"subject"})';
		} elsif ($1 eq 'p') {
			$sortsub .= '$task{$a}->{"priority"} <=> $task{$b}->{"priority"}';
		} elsif ($1 eq 'P') {
			$sortsub .= '$task{$b}->{"priority"} <=> $task{$a}->{"priority"}';
		} elsif ($1 eq 'b') {
			$sortsub .= '($task{$a}->{"bbe"}?$task{$a}->bbe:time+14*24*60*60) <=> ($task{$b}->bbe?$task{$b}->bbe:time+14*24*60*60)';
		} elsif ($1 eq 'B') {
			$sortsub .= '($task{$b}->{"bbe"}?$task{$b}->bbe:time+14*24*60*60) <=> ($task{$a}->bbe?$task{$a}->bbe:time+14*24*60*60)';
		} elsif ($1 eq 's') {
			$sortsub .= '$task{$a}->{"state"} <=> $task{$b}->{"state"}';
		} elsif ($1 eq 'S') {
			$sortsub .= '$task{$b}->{"state"} <=> $task{$a}->{"state"}';
		} elsif ($1 eq 'u') {
			$sortsub .= '$task{$a}->{"list"} cmp $task{$b}->list';
		} elsif ($1 eq 'U') {
			$sortsub .= '$task{$b}->{"list"} cmp $task{$a}->list';
		} elsif ($1 eq 't') {
			$sortsub .= '$a <=> $b || $a cmp $b';
		} elsif ($1 eq 'T') {
			$sortsub .= '$b <=> $a || $b cmp $a';
		}
		$sortsub .= ' || ';
	}
	$sortsub .= '$a <=> $b || -1; }';
	eval $sortsub;
	@sortedids = sort byformat (keys %task);
}

1;
