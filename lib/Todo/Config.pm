# WebTodo, a cgi-based todolist system
# Copyright (C) 1999-2000 British Broadcasting Corporation
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# See the COPYING file for details.

package Todo::Config;

$prefix="/usr/local/todo";

$tasks_dir="$prefix/data";
$dbmfile="index2.db";  # always in tasks_dir

# tasks older than this many days and marked 'done' will be moved to the
# old subdir when clearoutold.pl is run (from cron, perhaps)
$done_timeout = 7;

$config_dir="$prefix/lib";
$alias    = "$config_dir/alias";
$htgroup  = "$config_dir/htgroup";
$htpasswd = "$config_dir/htpasswd";
$views    = "$config_dir/views.cfg";
$prefs_dir= "$prefix/data/users";

$tmpl_dir="$prefix/templates";
$request_dir="$tmpl_dir/request";

$wwwserver="localhost";
$cgi_path = "/cgi-bin/todo";
$cgi_prefix="http://${wwwserver}${cgi_path}";
$list_cgi = "list";
$edit_cgi = "edit";
$add_cgi  = "add";
$config_cgi = "config";
$search_cgi = "search";
$merge_cgi = "merge";
$setcolour_cgi = "setcolour";

$httpd_user = "nobody";
$task_group = "nobody";

$defaultemail="engineers\@lists.example.com";

1;
