package Todo::General;

use Time::Local;
use POSIX;

# create a file only if it doesn't already exist
# returns 1 on success.

sub creat {
	my $fname = shift;
	my $file = POSIX::open($fname, &POSIX::O_CREAT | &POSIX::O_EXCL | &POSIX::O_WRONLY);
	if ($file) {
		POSIX::close($file);
		return 1;
	}
	return 0;
}

# parse_emaillist take a list of emails and return only the valid ones
# (comma-separated)

sub parse_emaillist {
	my $origlist = shift;
	my $x, @addies, $newlist;

	$origlist =~ s/;/,/g;
	@addies = split(/,/, $origlist);
	$newlist = "";
	foreach $x (@addies) {
		if ($x =~ /.*<(.*)>/) {
			$x = $1;
		}
		if ($x =~ /(.*)\(.*\)/) {
			$x = $1;
		}
		next if ($x !~ /[\w\-.]+@[\w\-.]*[\w.]+\.\w+/);
		$newlist .= $x.",";
	}
	$newlist =~ s/,$//;
	$newlist =~ s/\s//g;
	return $newlist;
}

# stringtotime

sub stringtotime {
	my ($y,$m,$d);
	$_ = $_[0];
	if($_ !~ /([0-9]+)[^0-9]([0-9]+)[^0-9]([0-9]+)/) {
		return 0;
	}
	($y,$m,$d) = /([0-9]+)[^0-9]([0-9]+)[^0-9]([0-9]+)/;
	if ($y < 70) { $y += 2000; }
	if ($y < 100) { $y += 1900; }
	$m -= 1;
	if (!$y) {
		($d,$m,$y) = (localtime())[3,4,5];
		#$m += 1;
		$y += 1900;
	}
	timelocal(0,0,0,$d,$m,$y);
}

# some handy subs

sub unescape {
	my($todecode) = @_;
	$todecode =~ tr/+/ /;       # pluses become spaces
	$todecode =~ s/%([0-9a-fA-F]{2})/pack("c",hex($1))/ge;
	return $todecode;
}

sub escape {
	my($toencode) = @_;
	$toencode=~s/([^a-zA-Z0-9_\-.])/uc sprintf("%%%02x",ord($1))/eg;
	return $toencode;
}

1;
