package Date::Format;
use POSIX;
use Exporter ();
@ISA = qw(Exporter);

@EXPORT=qw(time2str);

sub time2str {
	my $fmt = $_[0] || "%Y-%m-%d";
	my $time = $_[1] || time;
	my ($sec,$min,$hour,$mday,$mon,$year,$wday);
	($sec,$min,$hour,$mday,$mon,$year,$wday) = (localtime($time));
	POSIX::strftime($fmt,$sec,$min,$hour,$mday,$mon,$year,$wday);
}

1;
